import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { Chapter } from './chapter.entity';
import { v1 as uuid } from 'uuid';
import { ChapterRepository } from './chapter.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { BooksService } from 'src/book/books.service';
import { addChapterDto } from './addChapter.dto';
import { User } from 'src/user/user.entity';
import { deleteChapterDto } from './deleteChapter.dto';
import { updateChapterDto } from './updateChapter.dto';
@Injectable()
export class ChaptersService {
  constructor(
    @InjectRepository(ChapterRepository)
     private ChapterRepository: 
     ChapterRepository,
     private booksService:
     BooksService
  ) {}

  async getChapterById(id: number): Promise<Chapter>{
    return this.ChapterRepository.findOne(id,{select: ["id","index","book","content","title"]});
  }
  async getChaptersFromBookId(id: number): Promise<Chapter[]>{
    return this.ChapterRepository.find({where:{book: {id}}});
  }
  async getChapterFromBookId(idBook: number,index:number): Promise<Chapter>{
    return this.ChapterRepository.findOne({where:{book: {id:idBook},index: index}});
  }
  async addChapter(addChapterDto: addChapterDto,user: User): Promise<Chapter>{
    const book_founded = await this.booksService.getBookById(addChapterDto.book);
    if(book_founded && book_founded.user_author.id == user.id)
    {
      const newBook = await this.ChapterRepository.addChapter(addChapterDto,user,book_founded);
      return newBook;
    }
    else{
      throw new UnauthorizedException();
    }
  }

  async updateChapter(updateChapterDto: updateChapterDto,user: User): Promise<Chapter>{
    const book_founded = await this.booksService.getBookById(updateChapterDto.book);
    const chapter_founded = await this.ChapterRepository.findOne(updateChapterDto.id);
    if(chapter_founded && book_founded && book_founded.user_author.id == user.id)
    {
      await this.ChapterRepository.updateChapter(updateChapterDto);
      return this.getChapterById(updateChapterDto.id);
    }
    else{
      throw new UnauthorizedException();
    }

  }

  async deleteChapter(deleteChapterDto: deleteChapterDto,user: User): Promise<boolean>{
    const { id,book_id } = deleteChapterDto;
    console.log(deleteChapterDto);
    const book = await this.booksService.getBookById(book_id);
    if(book.user_author.id == user.id)
    {
      return this.ChapterRepository.delete(id) ? true : false;
    }
    else{
      throw new UnauthorizedException();
    }
  }
}