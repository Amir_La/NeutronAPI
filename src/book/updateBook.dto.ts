import { IsNotEmpty, IsString,IsNumber } from "class-validator";
export class updateBookDto {
  @IsNumber()
    id: number;
  @IsString()
    name: String;
  @IsString()
    author: String;
  @IsString()
    description: String;
}